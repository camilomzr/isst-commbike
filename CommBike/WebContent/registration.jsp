<%@ page contentType="text/html; charset=UTF-8" %>
<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113467793-4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      
      gtag('config', 'UA-113467793-4');
    </script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Registrarse - CommBike - Tu aplicación de movildiad rápida y sostenible.</title>
    <link rel="preconnect" href="https://www.google-analytics.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com" crossorigin>
    <link rel="preconnect" href="https://www.googletagmanager.com" crossorigin>
    <meta name="msapplication-TileColor" content="#206bc4"/>
    <meta name="theme-color" content="#206bc4"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="HandheldFriendly" content="True"/>
    <meta name="MobileOptimized" content="320"/>
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon"/>
    <meta name="description" content="Tabler comes with tons of well-designed components and features. Start your adventure with Tabler and make your dashboard great again. For free!"/>
    <meta name="twitter:image:src" content="https://preview.tabler.io/static/og.png">
    <meta name="twitter:site" content="@tabler_ui">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Tabler: Premium and Open Source dashboard template with responsive and high quality UI.">
    <meta name="twitter:description" content="Tabler comes with tons of well-designed components and features. Start your adventure with Tabler and make your dashboard great again. For free!">
    <meta property="og:image" content="https://preview.tabler.io/static/og.png">
    <meta property="og:image:width" content="1280">
    <meta property="og:image:height" content="640">
    <meta property="og:site_name" content="Tabler">
    <meta property="og:type" content="object">
    <meta property="og:title" content="Tabler: Premium and Open Source dashboard template with responsive and high quality UI.">
    <meta property="og:url" content="https://preview.tabler.io/static/og.png">
    <meta property="og:description" content="Tabler comes with tons of well-designed components and features. Start your adventure with Tabler and make your dashboard great again. For free!">
    <!-- CSS files -->
    <link href="./css/tabler.min.css?1616362010" rel="stylesheet"/>
    <link href="./css/tabler-flags.min.css?1616362010" rel="stylesheet"/>
    <link href="./css/tabler-payments.min.css?1616362010" rel="stylesheet"/>
    <link href="./css/tabler-vendors.min.css?1616362010" rel="stylesheet"/>
    <link href="./css/demo.min.css?1616362010" rel="stylesheet"/>
  </head>
  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="page page-center">
      <div class="container-tight py-4">
        <div class="text-center mb-4">
            <a href="."><img src="images/logo.jpg" height="36" alt=""></a>
        </div>
        <form class="card card-md" action="." method="get">
          <div class="card-body">
            <h2 class="card-title text-center mb-4">Crear nueva cuenta</h2>
            <div class="mb-3">
              <label class="form-label required">Nombre</label>
              <input type="text" class="form-control" placeholder="Introduce tu nombre">
            </div>
            <div class="mb-3">
              <label class="form-label required">Correo</label>
              <input type="email" class="form-control" placeholder="Introduce tu correo">
            </div>
            <div class="mb-3">
              <label class="form-label required">Contraseña</label>
              <div class="input-group input-group-flat">
                <input type="password" class="form-control"  placeholder="Introduce tu contraseña"  autocomplete="off">
                <span class="input-group-text">
                  <a href="#" class="link-secondary" title="Show password" data-bs-toggle="tooltip"><!-- Download SVG icon from http://tabler-icons.io/i/eye -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><circle cx="12" cy="12" r="2" /><path d="M22 12c-2.667 4.667 -6 7 -10 7s-7.333 -2.333 -10 -7c2.667 -4.667 6 -7 10 -7s7.333 2.333 10 7" /></svg>
                  </a>
                </span>
              </div>
            </div>
            <div class="mb-3">
              <label class="form-check ">
                <input type="checkbox" class="form-check-input"/>
                <span class="form-check-label required">Acepto la <a href="./terms-of-service.html" tabindex="-1"> política de privacidad</a>.</span>
              </label>
            </div>
            <div class="form-footer">
              <button type="submit" class="btn btn-primary w-100">Crear nueva cuenta</button>
            </div>
          </div>
        </form>
        <div class="text-center text-muted mt-3">
          ¿Ya tienes una cuenta? <a href="./login.jsp" tabindex="-1">Iniciar sesión</a>
        </div>
      </div>
    </div>
    <script
	src="https://browser.sentry-cdn.com/5.27.6/bundle.tracing.min.js"
	integrity="sha384-9Z8PxByVWP+gIm/rTMPn9BWwknuJR5oJcLj+Nr9mvzk8nJVkVXgQvlLGZ9SIFEJF"
	crossorigin="anonymous"
></script>
    <script>
      Sentry.init({
      	dsn: "https://8e4ad02f495946f888620f9fb99fd495@o484108.ingest.sentry.io/5536918",
      	release: "tabler@1.0.0-beta",
      	integrations: [
      		new Sentry.Integrations.BrowserTracing()
      	],
      
      	tracesSampleRate: 1.0,
      });
    </script>
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="./js/tabler.min.js?1616362010"></script>
  </body>
</html>