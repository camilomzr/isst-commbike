<%@ page contentType="text/html; charset=UTF-8" %>
<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>escanearQR - CommBike - Tu aplicación de movildiad rápida y sostenible.</title>
    <!-- CSS files -->
    <link href="css/tabler.min.css" rel="stylesheet"/>
    <link href="css/tabler-flags.min.css" rel="stylesheet"/>
    <link href="css/tabler-payments.min.css" rel="stylesheet"/>
    <link href="css/tabler-vendors.min.css" rel="stylesheet"/>
    <link href="css/demo.min.css" rel="stylesheet"/>
  </head>

  <header class="navbar navbar-expand-md navbar-dark d-print-none">
    <div class="container-xl">
      <jsp:include page="MenuUser.jsp" flush="true" />
    </div>
  </header>

  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="flex-fill d-flex flex-column justify-content-center py-4">
      <div class="container-tight py-6">
        <form class="card card-md" action="." method="get" autocomplete="off">
          <div class="card-body">
            <h2 class="card-title text-center mb-4">Escaenar QR de la bicicleta</h2>
            <div class="contenedor">
              <div class="titulo">Cámara             
 
              </div>
              <div>
                <script>
                  navigator.mediaDevices.getUserMedia({audio:false, video:true}).then((stream)=>{
                    console.log(stream)
                    let video = document.getElementById('video')
                    video.srcObject = stream
                    video.onloadedmetadata = (ev) => video.play()
                  }).catch((err)=>console.log(err))
                </script>
              </div>
          </div>
          </div>
        </form>
      </div>
      <div class="container-tight py-6">
        <form class="card card-md" action="." method="get" autocomplete="off">
          <div class="card-body">
            <h2 class="card-title text-center mb-4">Datos bicicleta escaneo manual</h2>
            <div class="mb-3">
              <label class="form-label">ID bicicelta</label>
              <input type="idBicicleta" class="form-control" placeholder="Introducir el ID de la bicicleta">
            </div>
            <div class="form-footer">
              <button type="submit" class="btn btn-primary w-100">Enter</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="js/tabler.min.js"></script>
  </body>
  
  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="flex-fill d-flex flex-column justify-content-center py-4">
      <div class="container-tight py-6">
        <!-- Stream video via webcam -->
        <div class="video-wrap">
          <video id="video" autoplay control></video>
        </div>

        <!-- Webcam video snapshot -->
        <canvas id="canvas" width="640" height="480"></canvas>
      </div>
    </div>
  </body>
</html>



