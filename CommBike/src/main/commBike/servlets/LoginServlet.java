package main.commBike.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// importar archivos DAO.java 

@WebServlet({"/LoginServlet", "/"})
public class LoginServlet extends HttpServlet {
    
        // falta login con base de datos y no asi
        private static final long serialVersionUID = 1L;
        private final String ADMIN_EMAIL = "root";
        private final String ADMIN_PASSWORD = "root";        
        
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp)
                           throws ServletException, IOException {
            String email = req.getParameter("email");
            String password = req.getParameter("password");
            Client user = ClientBuilder.newClient(new ClientConfig());
            // autenticacion1
            if( ADMIN_EMAIL.equals(email) && ADMIN_PASSWORD.equals(password) ) {        
                 req.getSession().setAttribute("admin", true);
                 List<TFG> tfgs  = user.target(URLHelper.getURL())
                        .request().accept(MediaType.APPLICATION_JSON)
                        .get(new GenericType<List<TFG>>() {});
                 req.setAttribute("tfgs", tfgs);
               getServletContext().getRequestDispatcher("/Admin.jsp")
                                  .forward(req,resp);
                return;
            }
            getServletContext().getRequestDispatcher("/index.html").forward(req,resp);
        }
}